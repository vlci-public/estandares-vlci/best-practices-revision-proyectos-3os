**Índice**  

 [[_TOC_]]  


# Introducción
En el presente documento se explica qué cosas tenemos que tener en cuenta en los proyectos que revisamos de 3os

# Documento Diseño
Cuando revisemos un documento de diseño, se tendrán en cuenta las siguientes consideraciones:

- `Arquitectura General`: Existirá un diagrama de flujo de datos que represente la arquitectura general de la solución con el end to end del proceso, desde los orígenes del dato (normalmente los sensores) hasta la persistencia de los mismos o su representación visual en diferentes cuadros de mando, etc.
- `Modelo de datos`: Existirá un diagrama de entidad que represente todas las Entidades Fiware y sus relaciones.
- `Entidades`: Las entidades usadas serán en la medida de lo posible entidades estándar Fiware. Excepcionalmente si se quiere modelar algo que no existe en Fiware, se crearán entidades ad-hoc.

# Estándares de Integración
A la hora de integrar en plataforma:
- `Persistencia`: Todas las entidades con datos cambiantes en el tiempo deberán ser persistidas mediante suscripción de ContextBroker al Mongo. [En el siguiente link](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/mongo/README.md) se explican las buenas prácticas de integración en Mongo.

Obviamente, a parte de lo aquí descrito, se seguirán todos los estándares de integración en plataforma descritos en los siguientes apartados:

- `IoT Agent`: [Aquí se encuentran las buenas prácticas relativas a los elementos del IoT Agent](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/iotagents/README.md)
- `Orion Context Broker`: [Aquí se encuentran las buenas prácticas relativas a las entidades y suscripciones del Orion Context Broker](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/contextbroker/README.md)
- `CEP`: [Aquí se encuentran las buenas prácticas relativas a los elementos del CEP](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/cep/README.md)
- `Cygnus`: [Aquí se encuentran las buenas prácticas relativas a los elementos del Cygnus](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/cygnus/README.md)
- `CKAN`: [Aquí se encuentran las buenas prácticas relativas a los elementos del CKAN](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/ckan/README.md)
- `Mongo`: [Aquí se encuentran las buenas prácticas relativas a los elementos del Mongo](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/mongo/README.md)
- `GIS Municipal`: El GIS no es un componente específico de la plataforma, pero sí que hay una serie de buenas prácticas para comunicarse con él mediante suscripciones del ContextBroker. [Aquí se encuentran las buenas prácticas relativas a toda esa comunicación con el GIS](https://gitlab.com/vlci-public/estandares-vlci/best-practices-plataforma-vlci/-/blob/main/suscripciones-gis/README.md)

# Trabajos a realizar por el equipo de Integración del SCI
En el momento que nos llegue un primero documento de diseño de una empresa externa debemos realizar las siguientes tareas.
- `Crear EPIC`: En Jira, crear un EPIC asociado a dicho proyecto.
- `Crear Ticket Mongo`: En Jira, se creará un ticket para analizar qué entidades de deben llevar a Mongo y crear la suscripción. Este ticket debe pertenecer al EPIC creado anteriormente.
- `Crear Ticket Modelo de datos`: En Jira, crearemos el ticket para añadir la nueva información de las entidades a nuestro inventario de entidades de gitlab (https://gitlab.com/vlci-public/models-dades)

